# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_03_31_193541) do

  create_table "account_settings", force: :cascade do |t|
    t.string "name"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "duration", default: 60
    t.string "timezone"
    t.string "slug"
    t.string "address"
    t.index ["account_id"], name: "index_account_settings_on_account_id"
    t.index ["slug"], name: "index_account_settings_on_slug"
  end

  create_table "accounts", force: :cascade do |t|
    t.string "remote_id"
    t.string "auth_token"
    t.string "calendar_id"
    t.string "calendar_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "available_periods", force: :cascade do |t|
    t.integer "day_of_week"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "start_hour"
    t.integer "start_minute"
    t.integer "end_hour"
    t.integer "end_minute"
    t.index ["account_id"], name: "index_available_periods_on_account_id"
  end

  create_table "form_registries", force: :cascade do |t|
    t.string "form_id"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_form_registries_on_account_id"
    t.index ["form_id"], name: "index_form_registries_on_form_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "account_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["account_id"], name: "index_users_on_account_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
