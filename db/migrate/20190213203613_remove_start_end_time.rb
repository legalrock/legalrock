class RemoveStartEndTime < ActiveRecord::Migration[5.2]
  def change
    remove_column :available_periods, :start_time
    remove_column :available_periods, :end_time
  end
end
