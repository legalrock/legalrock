class AddAddressToSettings < ActiveRecord::Migration[5.2]
  def change
    add_column :account_settings, :address, :string
  end
end
