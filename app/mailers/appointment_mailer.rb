class AppointmentMailer < ApplicationMailer
  def confirmation
    @date = Time.iso8601(params[:date])
    @firm_name = params[:firm_name]
    @person_name = params[:person_name]
    @address = params[:address]
    email = params[:email]
    mail(to: email)
  end

  def created
    @date = Time.iso8601(params[:date])
    @person_name = params[:person_name]
    @text = params[:text]
    email = params[:email]
    mail(to: email)
  end
end
