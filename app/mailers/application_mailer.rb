class ApplicationMailer < ActionMailer::Base
  default from: 'banda@legalrock.es'
  layout 'mailer'
end
