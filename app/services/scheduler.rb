class Scheduler
  TimeWindow = Struct.new(:start, :end)

  class << self
    def query_free_slots(account, duration_minutes, date_from, date_to)
      available_slots = find_availability(account, date_from, date_to, duration_minutes)
      duration_slots = chunk_slots(available_slots, duration_minutes, account)

      build_time_windows(duration_slots, duration_minutes)
    end

    private

    def find_availability(account, date_from, date_to, duration_minutes)
      time_windows = constraint_with_available_periods(account, date_from, date_to)
      kloudless_account = Kloudless::Account.new(account.remote_id, account.auth_token)
      kloudless_calendar = Kloudless::Calendar.new(account.calendar_id, account.calendar_name)
      Kloudless::Availability.find(kloudless_account, kloudless_calendar, time_windows, duration_minutes)
    end

    def constraint_with_available_periods(account, date_from, date_to)
      time_windows = []
      date_from.to_datetime.upto(date_to.to_datetime) do |day|
        time_window = constraint_time_for_day(day, account)
        time_windows << time_window unless time_window.nil?
      end
      time_windows
    end

    def constraint_time_for_day(day, account)
      period_day_of_week = account.available_period.find { |period| period.day_of_week == day.cwday }
      return nil if period_day_of_week.nil?

      {
        start: account.account_setting.new_time_with_zone(day.year, day.month, day.day, period_day_of_week.start_hour, period_day_of_week.start_minute),
        end: account.account_setting.new_time_with_zone(day.year, day.month, day.day, period_day_of_week.end_hour, period_day_of_week.end_minute)
      }
    end

    def chunk_slots(available_slots, duration, account)
      duration_slots = []
      available_slots.each do |available_slot|
        start = account.account_setting.parse_time_in_zone(available_slot['start'])

        while duration_fits_in_slot?(start, available_slot, duration)
          duration_slots << start
          start += duration.minutes
        end
      end
      duration_slots
    end

    def duration_fits_in_slot?(start, slot, duration)
      start + duration.minutes <= slot['end']
    end

    def build_time_windows(dates, duration_minutes)
      duration = duration_minutes.minutes
      dates.map { |d| TimeWindow.new(d, d + duration) }
    end
  end
end
