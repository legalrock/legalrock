class UpdateAvailablePeriods
  class << self
    DAYS_OF_WEEK = (1..7).freeze

    def call(account, params)
      Rails.logger.info("Received requesy to update available periods for account #{account}")
      persisted_periods = AvailablePeriod.where(account: account)
      present_days, missing_days = DAYS_OF_WEEK.partition { |day| params[day.to_s].present? }
      persisted_days = persisted_periods.map(&:day_of_week)

      create = periods_to_create(persisted_days, present_days, params, account)
      update = periods_to_update(persisted_periods, persisted_days, present_days, params)
      delete = periods_to_delete(missing_days, persisted_periods)

      persist_changes(create, update, delete)
    end

    private

    def periods_to_create(persisted_days, requested_days, params, account)
      days_to_create = requested_days - persisted_days
      Rails.logger.info("Creating available periods #{days_to_create}")

      days_to_create.map do |day|
        AvailablePeriodForm.new.tap do |ap|
          ap.assign_attributes(day_of_week: day, start_time: params["start_#{day}"], end_time: params["end_#{day}"], account: account)
        end
      end
    end

    def periods_to_update(persisted_periods, persisted_days, requested_days, params)
      days_to_update = requested_days & persisted_days
      Rails.logger.info("Updating available periods #{days_to_update}")
      days_to_update.map do |day|
        period = persisted_periods.find { |p| p.day_of_week == day }
        AvailablePeriodForm.from_model(period).tap do |ap|
          ap.assign_attributes(start_time: params["start_#{day}"], end_time: params["end_#{day}"])
        end
      end
    end

    def periods_to_delete(missing_days, persisted_periods)
      Rails.logger.info("Deleting available periods #{missing_days}")
      missing_days.map do |day|
        persisted_periods.find { |p| p.day_of_week == day }
      end.compact
    end

    def persist_changes(create, update, delete)
      to_save = create + update
      return to_save unless to_save.all?(&:valid?)

      ActiveRecord::Base.transaction do
        to_save.each(&:save!)
        delete.each(&:destroy!)
      end

      to_save
    end
  end
end
