class CreateEvent
  class Error < StandardError; end
  class CalendarError < CreateEvent::Error; end
  class TimeNotAvailableError < CreateEvent::Error; end

  class << self
    def call(account, answers)
      log_create(account, answers)

      event = build_event(account, answers)
      validate_event!(event, account)
      Kloudless::Events.create(event)

      send_confirmation(account, answers)
      send_created(account, answers)
    rescue Kloudless::Error, Kloudless::NetException => e
      Rails.logger.error("Rescued error #{e.message} caused by #{e.cause}")
      raise CreateEvent::CalendarError, "Error Creating the event with #{answers}, #{account}"
    end

    private

    def validate_event!(event, account)
      available_times = Kloudless::Availability.find(
        event.account,
        event.calendar,
        [{ start: event.time_start, end: event.time_end }],
        account.account_setting.duration
      )

      return if available_times.any?

      Rails.logger.error("Slot not available for #{event}")
      raise CreateEvent::TimeNotAvailableError, "Slot not available for #{event}"
    end

    def send_confirmation(account, answers)
      AppointmentMailer.with(
        date: answers.date.iso8601,
        email: answers.email,
        firm_name: account.account_setting.name,
        person_name: answers.name,
        address: account.account_setting.full_address
      ).confirmation.deliver_later
    end

    def send_created(account, answers)
      AppointmentMailer.with(
        date: answers.date.iso8601,
        email: account.user.email,
        text: answers.text,
        person_name: answers.name
      ).created.deliver_later
    end

    def build_event(account, answers)
      kaccount = Kloudless::Account.new(account.remote_id, account.auth_token)
      kcalendar = Kloudless::Calendar.new(account.calendar_id, account.calendar_name)
      Kloudless::Event.new("Cita con #{answers.name}", kaccount, kcalendar, answers.date, answers.date + account.account_setting.duration.minutes, answers.name, answers.email, answers.text)
    end

    def log_create(account, answers)
      Rails.logger.info("[CREATE EVENT] account_id: #{account.id}, name: #{account.account_setting.name}, with #{answers}")
    end
  end
end
