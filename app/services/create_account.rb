class CreateAccount
  class Error < StandardError; end

  class << self
    def call(name)
      account = Account.create!
      AccountSetting.create!(account: account, name: name, timezone: AccountSetting::DEFAULT_TIMEZONE)
      assign_default_available_periods(account)
      account
    end

    def add_remote(account, kloudless_account_id, kloudless_auth_token)
      Kloudless::Auth.verify_token(kloudless_account_id, kloudless_auth_token)

      account.update!(
        remote_id: kloudless_account_id,
        auth_token: kloudless_auth_token
      )

      account
    rescue Kloudless::Error => e
      Rails.logger.error("an error occurred verifying the token#{e.message}")
      raise CreateAccount::Error, e.message
    end

    def add_calendar(account, id, name, timezone)
      ActiveRecord::Base.transaction do
        account.update!(
          calendar_id: id,
          calendar_name: name
        )

        account.account_setting.update!(
          timezone: timezone
        )
      end
    end

    private

    def assign_default_available_periods(account)
      mon_to_fri = (1..5)
      account.available_period = mon_to_fri.map { |day_of_week| AvailablePeriod.default(day_of_week) }
    end
  end
end
