module ErrorsForTablesHelper
  def add_error_attrs(model, fields, html_attrs)
    selected_errors = (model.errors.keys & fields)
    return html_attrs if selected_errors.empty?

    html_attrs[:class] << ' is-invalid'
    tootip = { data: { toggle: 'tooltip', placement: 'bottom' }, title: model.errors[selected_errors.first].join(', ') }

    html_attrs.merge(tootip)
  end
end
