class AppointmentsController < ApplicationController
  layout 'unauthenticated'

  def show
    log_access
    update
  end

  def update
    answers = build_answers
    @form = StepFactory.build(answers, account)

    session[:appointment_form] = answers.to_h

    if answers.valid?
      create_event(account, answers)
      session[:appointment_form] = nil
    end

    render :show
  end

  private

  def create_event(account, answers)
    CreateEvent.call(account, answers)
  rescue CreateEvent::CalendarError => e
    Rails.logger.error("Error creating event #{e.message}")
  end

  def build_answers
    answers = AppointmentForm.from_h(session[:appointment_form])
    answers.read_params(params.permit(appointment: {})['appointment'])
    answers.date_from = params['from']
    answers
  end

  def log_access
    Rails.logger.info("[QUERY SLOTS] account: #{account.id}, name: #{account.account_setting.name}, params: #{params}")
  end

  class StepFactory
    def self.build(answers, account)
      return DateSelectionStep.new(answers, account) if answers.date.nil?
      return PersonalDataStep.new(answers, account) unless answers.valid?

      ConfirmationStep.new(answers, account)
    end
  end

  class Step
    def initialize(answers, account)
      @answers = answers
      @account = account
    end

    def account_name
      @account_name = @account.account_setting.name
    end
  end

  class ConfirmationStep < Step
    def partial_name
      'confirmation'
    end

    def email
      @answers.email
    end

    def date
      @answers.date
    end

    def address
      "#{account_name}, #{@account.account_setting.address}"
    end
  end

  class PersonalDataStep < Step
    def partial_name
      'personal_data'
    end

    def date
      @answers.date
    end

    def account_id
      @account.id
    end
  end

  class DateSelectionStep < Step
    N_VISIBLE_DAYS = 5

    def initialize(answers, account)
      setup_query_dates(answers)
      super
    end

    def partial_name
      'date_selection'
    end

    def account_id
      @account.id
    end

    def next_period
      @date_to + 1.day
    end

    def days_to_show
      days = []
      @date_from.to_datetime.upto(@date_to.to_datetime) { |d| days << d }
      days
    end

    def slots_by_day_of_week
      slots.group_by { |d| d.start.wday + 1 }
    end

    def n_rows
      slots_by_day = slots_by_day_of_week
      slots_by_day.values.any? ? slots_by_day.values.map(&:count).max : 1
    end

    private

    def setup_query_dates(answers)
      date_from = Time.now.utc.beginning_of_day.advance(days: 1)

      if answers.date_from.present?
        begin
          date_from = Time.parse(answers.date_from).utc
        rescue ArgumentError
          Rails.logger.error("Error parsing param #{answers.date_from}")
        end
      end

      assign_dates(date_from)
    end

    def assign_dates(date_from)
      @date_from = date_from
      @date_to = @date_from + N_VISIBLE_DAYS.days
    end

    def slots
      return @slots unless @slots.nil?

      @slots = Scheduler.query_free_slots(@account, @account.account_setting.duration, @date_from, @date_to)
      # step = Proc.new {|step| Scheduler::TimeWindow.new(@date_from+step.days, @date_from+step.days+1.hour) }
      # @slots = [ step.call(0), step.call(0), step.call(0), step.call(1), step.call(1), step.call(1), step.call(2), step.call(3), step.call(3), step.call(3), step.call(3), step.call(4), step.call(4), step.call(4), step.call(5) ]
      # slots = [ step.call(1), step.call(1), step.call(1), step.call(3), step.call(3), step.call(4), step.call(4), step.call(4), step.call(4), step.call(5), step.call(5), step.call(5), step.call(6) ]
    end
  end

  def account
    @account ||= Account.find_by_slug(params.require(:slug))
  end
end
