class Account < ApplicationRecord
  has_many :available_period
  has_one :account_setting
  has_one :user

  def connected?
    remote_id.present?
  end

  def self.find_by_slug(slug)
    accounts = Account.joins(:account_setting).where(account_settings: { slug: slug })
    raise ActiveRecord::RecordNotFound, "Account for slug #{slug} not found" if accounts.empty?

    accounts.first
  end
end
