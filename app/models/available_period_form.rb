class AvailablePeriodForm
  include ActiveModel::Validations
  include ActiveRecord::AttributeAssignment

  TIME_FORMAT = /\A([0-9]|0[0-9]|1[0-9]|2[0-3]):([0-5][0-9])\z/.freeze

  validates :start_time, presence: true, format: { with: TIME_FORMAT }
  validates :end_time, presence: true, format: { with: TIME_FORMAT }

  validate :object_valid?

  attr_accessor :day_of_week, :start_time, :end_time, :account
  attr_writer :object

  def self.from_model(available_period)
    ap = AvailablePeriodForm.new

    ap.assign_attributes(
      day_of_week: available_period.day_of_week,
      start_time: humanize_time(available_period.start_hour, available_period.start_minute),
      end_time: humanize_time(available_period.end_hour, available_period.end_minute),
      account: available_period.account,
      object: available_period
    )
    ap
  end

  def self.from_models(available_periods)
    available_periods.map { |ap| AvailablePeriodForm.from_model(ap) }
  end

  def save!
    as_available_period.save!
  end

  private

  def object_valid?
    @object = as_available_period
    valid = @object.valid?

    @object.errors.each do |attribute, message|
      errors.add(attribute, message)
    end

    valid
  end

  def self.humanize_time(hour, minute)
    return '' if hour.nil? || minute.nil?
    "#{hour.to_s.rjust(2, '0')}:#{minute.to_s.rjust(2, '0')}"
  end

  def as_available_period
    available_period = @object || AvailablePeriod.new

    if(start_time.present?)
      start_hour, start_minute = start_time.match(TIME_FORMAT) do |m|
        m.captures
      end
    end

    if(end_time.present?)
      end_hour, end_minute = end_time.match(TIME_FORMAT) do |m|
        m.captures
      end
    end

    # start_hour, start_minute = start_time.match(TIME_FORMAT).captures if start_time.present?
    # end_hour, end_minute = end_time.match(TIME_FORMAT).captures if end_time.present?


    available_period.assign_attributes(
      day_of_week: day_of_week,
      start_hour: start_hour,
      end_hour: end_hour,
      start_minute: start_minute,
      end_minute: end_minute,
      account: account
    )

    available_period
  end
end
