class AppointmentForm
  include ActiveModel::Validations

  attr_accessor :date, :name, :email, :text, :date_from

  validates_presence_of :date, :name, :email
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  validate :date_is_date
  validates_length_of :text, maximum: 250

  def to_h
    { date: @date&.iso8601, name: @name, email: @email, text: @text }
  end

  def read_params(params)
    return if params.blank?

    if params['reset'] == 'true'
      reset_values
    else
      assign_values(params)
    end
  end

  def self.from_h(hash)
    form = AppointmentForm.new
    return form if hash.nil?

    form.date = Time.iso8601(hash['date']) unless hash['date'].nil?
    form.name = hash['name']
    form.email = hash['email']
    form.text = hash['text']
    form
  end

  def to_s
    "<AppointmentForm name:#{name}, date:#{date}, email:#{email}, text: #{text}, date_from:#{date_from}>"
  end

  private

  def assign_values(params)
    @date = Time.iso8601(params['selected-date']) unless params['selected-date'].nil?
    @name = params['name'] unless params['name'].nil?
    @email = params['email'] unless params['email'].nil?
    @text = params['text'] unless params['text'].nil?
  end

  def reset_values
    @date = nil
    @name = nil
    @email = nil
    @text = nil
  end

  def date_is_date
    errors.add(:date, :invalid) unless date.instance_of?(Time)
  end
end
