class AvailablePeriod < ApplicationRecord
  belongs_to :account

  validates :end_hour, presence: true
  validates :end_minute, presence: true
  validates :start_hour, presence: true
  validates :start_minute, presence: true

  validate :start_before_end, if: :time_present?

  def self.default(day_of_week)
    new(start_hour: 9, start_minute: 0, end_hour: 17, end_minute: 0, day_of_week: day_of_week)
  end

  private

  def start_before_end
    invalid_h = end_hour < start_hour
    invalid_m = end_hour == start_hour && end_minute <= start_minute

    return unless invalid_h || invalid_m

    errors.add(:end_hour, :within)
  end

  def time_present?
    start_hour.present? && start_minute.present? && end_hour.present? && end_minute.present?
  end

  class << self
    def find_by_account(account_id)
      where(account_id: account_id)
    end
  end
end
