(() => {
  stimulus.register("select_calendar", class extends Stimulus.Controller {
    static get targets() {
      return [ "name", "timezone" ]
    }

    submit(event) {
      event.preventDefault();
      var selected = document.querySelector('input[type="radio"][checked="checked"]');
      var name = selected.dataset['name']
      this.nameTarget.value = name;
      var timezone = selected.dataset['timezone']
      this.timezoneTarget.value = timezone;
      this.element.submit();
    }
  })
})()

