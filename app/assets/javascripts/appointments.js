(() => {
  stimulus.register("appointments", class extends Stimulus.Controller {
    static get targets() {
      return [ "date", "reset" ]
    }
    // connect() {
    //    console.log(this.dateTarget)
    //  }

    submit(event) {
      var data = event.target.dataset['value']
      this.dateTarget.value = data;
      this.element.submit();
    }

    reset(event) {
      this.resetTarget.value = true;
      this.element.submit();
    }

  })
})()
