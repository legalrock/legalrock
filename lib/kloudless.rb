require 'kloudless/configuration'
require 'kloudless/auth'
require 'kloudless/account'
require 'kloudless/calendar'
require 'kloudless/calendars'
require 'kloudless/availability'
require 'kloudless/event'
require 'kloudless/events'
require 'kloudless/error'

module Kloudless
  class << self
    attr_writer :configuration
  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.reset
    @configuration = Configuration.new
  end

  def self.configure
    yield(configuration)
  end
end
