module Kloudless
  Account = Struct.new(:id, :auth_token) do
    def to_s
      "Account(#{id})"
    end

    def self.dev
      new('dev_account_id', 'dev_auth_token')
    end
  end
end
