module Kloudless
  Event = Struct.new(:name, :account, :calendar, :time_start, :time_end, :person_name, :person_email, :description) do
    def to_s
      "name: #{name}, account: #{account}, calendar: #{calendar}, time_start: #{time_start}, time_end: #{time_end}, person_name: #{person_name}, person_email: #{person_email}, description: #{description}"
    end
  end
end
