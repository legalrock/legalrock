module Kloudless
  Calendar = Struct.new(:id, :name, :timezone) do
    def to_s
      "Calendar(#{id}, #{name})"
    end

    def self.dev
      new('dev_calendar_id', 'calendar for development', 'Europe/Madrid')
    end
  end
end
