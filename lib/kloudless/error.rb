module Kloudless
  class Error < StandardError
  end

  class NetException < Error
  end
end
