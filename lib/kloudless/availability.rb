require 'net/http'

module Kloudless
  # https://developers.kloudless.com/docs/v1/calendar#calendar-availability
  class Availability
    def self.find(account, calendar, time_windows, meeting_duration_minutes)
      return [] if Kloudless.configuration.app_id.nil?

      Rails.logger.info("Requesting availability for #{account} in #{calendar} with #{time_windows}")
      uri = URI("https://api.kloudless.com/v1/accounts/#{account.id}/cal/availability")
      req = Net::HTTP::Post.new(uri)
      req.body = {
        calendars: [calendar.id],
        meeting_duration: meeting_duration_minutes.minutes.iso8601,
        constraints: {
          "time_windows": time_windows.map do |t|
            { "start": t[:start].iso8601, "end": t[:end].iso8601 }
          end
        }
      }.to_json

      req['Content-Type'] = 'application/json'
      req['Authorization'] = "Bearer #{account.auth_token}"

      http = Net::HTTP.new(uri.hostname, uri.port)
      http.use_ssl = true
      begin
        res = http.start { |conn| conn.request(req) }
      rescue StandardError => e
        Rails.logger.error("Network Error getting availability #{e.message}")
        raise Kloudless::NetException
      end

      body = JSON.parse(res.body)

      if body['error_code'].present?
        Rails.logger.error("Remote server error received: #{body}")
        raise Kloudless::Error
      end

      JSON.parse(res.body)['time_windows']
    end
  end
end
