require 'net/http'

module Kloudless
  # https://developers.kloudless.com/docs/v1/authentication#oauth-2.0
  class Auth
    # https://developers.kloudless.com/docs/v1/authentication#header-verify-the-token
    def self.verify_token(id, access_token)
      return if Kloudless.configuration.app_id.nil?

      Rails.logger.info("Verifying token id:#{id}, token: #{access_token}")

      uri = URI('https://api.kloudless.com/v1/oauth/token')
      req = Net::HTTP::Get.new(uri)
      req['Content-Type'] = 'application/json'
      req['Authorization'] = "Bearer #{access_token}"

      http = Net::HTTP.new(uri.hostname, uri.port)
      http.use_ssl = true
      begin
        res = http.start { |conn| conn.request(req) }
      rescue StandardError => e
        Rails.logger.error("Network Error verifying token #{access_token}:  #{e.message}")
        raise Kloudless::NetException, 'Network error verifying token'
      end

      body = JSON.parse(res.body)

      if body['error'].present?
        Rails.logger.error("Remote server error received: #{body}")
        raise Kloudless::Error, 'The token has been revoked'
      end

      raise Kloudless::Error, 'Error verifying token: app_id does not match' if body['client_id'] != Kloudless.configuration.app_id
    end
  end
end
