Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'setup_account/index', to: 'setup_account#index'
  post 'setup_account/connect', to: 'setup_account#connect'
  post 'setup_account/calendar', to: 'setup_account#calendar'
  get 'welcome/index', to: 'welcome#index'

  resources :appointments, only: [:show, :update], param: :slug, path: :citas
  resources :account_profiles, only: [:index, :update]
  resources :legal_static, only: [:index], path: 'legal'

  devise_for :users, controllers: {
    registrations: 'users/registrations',
    sessions: 'users/sessions',
    passwords: 'users/passwords'}

  authenticated :user do
    root 'account_profiles#index', as: :authenticated_root
  end

  root 'welcome#index'
end
