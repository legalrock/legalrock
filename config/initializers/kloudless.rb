require 'kloudless'

Kloudless.configure do |config|
  config.app_id = Rails.application.config_for(:kloudless)['app_id']
  config.scope = Rails.application.config_for(:kloudless)['scope']
end
