# Legalrock

## Configuration

Legalrock uses [kloudless](https://kloudless.com/) to provide the integration with the calendars, in order to connect with kloudless api the following env variables must be present:

* `APP_ID`: Kloudless application’s App ID

The keys to send emails via [SendGrid](https://sendgrid.com/):

* `SENDGRID_USERNAME` `SENDGRID_PASSWORD`

The secret keys that [devise](https://github.com/plataformatec/devise) needs to hash tokens and passwords

* `DEVISE_SECRET_KEY` `DEVISE_PEPPER`




## Configure development environment

### Environment variables

The project imports the gem [dotenv](https://github.com/bkeepers/dotenv) that allows declaring the environment variables in a file called `.env` in the root folder.

### Installing dependencies

This project uses ruby and also javascript. The following system dependencies are required:

- ruby interpreter version [.ruby-version](.ruby-version)
- [nodejs](https://nodejs.org/en/download/) version 8.10 or greater
- [bundler](https://bundler.io/) as ruby package manager
- [yarn](https://yarnpkg.com/lang/en/docs/install) as javascript package manager

After that execute the following commands to install the dependencies:

```bash
$> bundle install
```

```bash
$> yarn install
```

### Running the tests

```bash
$> bundle exec rails test
```











~~~~




















