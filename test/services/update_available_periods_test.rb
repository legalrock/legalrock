require 'test_helper'

class UpdateAvailablePeriodsTest < ActiveSupport::TestCase
  setup do
    @account = accounts(:one)
  end

  test 'add error to model when the format is invalid for a new period' do
    params = {
      'start_3' => 'invalid time',
      'end_3' => 'invalid time',
      '3' => '3'
    }

    available_periods = UpdateAvailablePeriods.call(@account, params)

    assert_equal(['no es válido'], available_periods.first.errors[:end_time])
    assert_equal(['no es válido'], available_periods.first.errors[:start_time])

    refute(period_exists?(day_of_week: 3))
  end

  test 'add error to model when the format is invalid for an existing period' do
    params_create = { '1' => '1', 'start_1' => '20:00', 'end_1' => '21:00' }
    params_update = { '1' => '1', 'start_1' => 'invalid_time', 'end_1' => 'invalid_time', }

    UpdateAvailablePeriods.call(@account, params_create)
    available_periods = UpdateAvailablePeriods.call(@account, params_update)

    assert_equal(['no es válido'], available_periods.first.errors[:end_time])
    assert_equal(['no es válido'], available_periods.first.errors[:start_time])

    assert_equal(20, find_periods(day_of_week: 1).first.start_hour)
  end

  test 'add error when the time is not present' do
    params = { '3' => '3' }

    available_periods = UpdateAvailablePeriods.call(@account, params)

    assert_equal(:blank, available_periods.first.errors.details[:end_time].first[:error])
    assert_equal(:blank, available_periods.first.errors.details[:start_time].first[:error])

    refute(period_exists?(day_of_week: 3))
  end

  test 'add error when the underlying model is not valid' do
    invalid_params = { '1' => '1', 'start_1' => '22:00', 'end_1' => '21:00' }

    updated = UpdateAvailablePeriods.call(@account, invalid_params)

    assert(updated.first.errors.include?(:end_hour))
  end

  test 'saves the periods' do
    AvailablePeriod.delete_all

    params = {
      '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00',
      '5' => '5', 'start_5' => '15:00', 'end_5' => '16:00',
      '6' => '', 'start_6' => '', 'end_6' => ''
    }

    UpdateAvailablePeriods.call(@account, params)

    updated = find_periods

    assert_equal(2, updated.count)

    assert_equal(1, updated[0].day_of_week)
    assert_equal(12, updated[0].start_hour)
    assert_equal(0, updated[0].start_minute)
    assert_equal(13, updated[0].end_hour)
    assert_equal(0, updated[0].end_minute)

    assert_equal(5, updated[1].day_of_week)
    assert_equal(15, updated[1].start_hour)
    assert_equal(0, updated[1].start_minute)
    assert_equal(16, updated[1].end_hour)
    assert_equal(0, updated[1].end_minute)
  end

  test 'updates the dates' do
    params = { '1' => '1', 'start_1' => '20:00', 'end_1' => '21:00' }

    UpdateAvailablePeriods.call(@account, params)

    updated = find_periods.first

    assert_equal(1, updated.day_of_week)
    assert_equal(20, updated.start_hour)
    assert_equal(0, updated.start_minute)
    assert_equal(21, updated.end_hour)
    assert_equal(0, updated.end_minute)
  end

  test 'deletes if a period is disabled' do
    params = { '1' => '', 'start_1' => '', 'end_1' => '' }
    UpdateAvailablePeriods.call(@account, params)

    refute(period_exists?(day_of_week: 1))
  end

  test 'rollbacks changes when an error is detected' do
    params_create = { '1' => '1', 'start_1' => '20:00', 'end_1' => '21:00' }

    params_update = {
      '1' => '', 'start_1' => '', 'end_1' => '',
      '2' => '2', 'start_2' => '20:00', 'end_2' => '19:00'
    }

    UpdateAvailablePeriods.call(@account, params_create)
    UpdateAvailablePeriods.call(@account, params_update)

    assert(period_exists?(day_of_week: 1))
  end

  def period_exists?(query)
    AvailablePeriod.exists?(query.merge(account: @account))
  end

  def find_periods(query={})
    AvailablePeriod.where(query.merge(account: @account))
  end
end
