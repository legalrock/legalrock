require 'test_helper'

class SchedulerTest < ActiveSupport::TestCase
  setup do
    @any_account = Kloudless::Account.new('id', 'auth_token')
    @any_calendar = Kloudless::Calendar.new('id', 'name')
    @account = Account.new(
      remote_id: @any_account.id,
      auth_token: @any_account.auth_token,
      calendar_id: @any_calendar.id,
      calendar_name: @any_calendar.name
    )
    @account.account_setting = AccountSetting.new(timezone: 'Europe/Madrid')
    @duration = 60
    @date_from = Time.utc(2017, 3, 22, 0, 0, 0)
    @date_to = Time.utc(2017, 3, 25, 0, 0, 0)
  end

  test '#free_slots splits a large time window in chunks' do
    Kloudless::Availability.stubs(:find).returns(
      [{ 'start' => '2017-03-20T09:00:00Z', 'end' => '2017-03-20T13:00:00Z' }]
    )

    result = Scheduler.query_free_slots(@account, @duration, @date_from, @date_to)

    assert_equal('2017-03-20T10:00:00+01:00', result[0].start.iso8601)
    assert_equal('2017-03-20T11:00:00+01:00', result[1].start.iso8601)
    assert_equal('2017-03-20T12:00:00+01:00', result[2].start.iso8601)
    assert_equal('2017-03-20T13:00:00+01:00', result[3].start.iso8601)
  end

  test '#free_slots obtain slots from different time windows' do
    Kloudless::Availability.stubs(:find).returns(
      [{ 'start' => '2017-03-20T09:00:00Z', 'end' => '2017-03-20T11:00:00Z' },
       { 'start' => '2017-03-20T015:00:00Z', 'end' => '2017-03-20T18:00:00Z' }]
    )

    result = Scheduler.query_free_slots(@account, @duration, @date_from, @date_to)

    assert_equal('2017-03-20T10:00:00+01:00', result[0].start.iso8601)
    assert_equal('2017-03-20T11:00:00+01:00', result[1].start.iso8601)
    assert_equal('2017-03-20T16:00:00+01:00', result[2].start.iso8601)
    assert_equal('2017-03-20T17:00:00+01:00', result[3].start.iso8601)
  end

  test '#free_slots uses the available periods of the account' do
    @account.available_period = [AvailablePeriod.create!(start_hour: 12, start_minute: 0, end_hour: 20, end_minute: 0, day_of_week: @date_from.wday, account: accounts(:one))]

    Kloudless::Availability.expects(:find).with(
      @any_account,
      @any_calendar,
      [{ start: Time.new(2017, 3, 22, 12, 0, 0, '+01:00'), end: Time.new(2017, 3, 22, 20, 0, 0, '+01:00') }],
      @duration
    ).returns(
      [{ 'start' => '2017-03-22T12:00:00Z', 'end' => '2017-03-22T20:00:00Z' }]
    )

    Scheduler.query_free_slots(@account, @duration, @date_from, @date_to)
  end

  test '#free_slots do not request times if available periods are not present' do
    two_slots = [{ 'start' => '2017-03-22T09:00:00Z', 'end' => '2017-03-22T11:00:00Z' }]
    periods = (1..7).map { |cwday| AvailablePeriod.create(start_hour: 12, start_minute: 0, end_hour: 13, end_minute: 0, day_of_week: cwday, account: accounts(:one)) }
    @account.available_period = periods.reject { |e| e.day_of_week == 4 }

    second_call_without23 = [{ start: '2017-03-22T12:00:00+01:00', end: '2017-03-22T13:00:00+01:00' }, { start: '2017-03-24T12:00:00+01:00', end: '2017-03-24T13:00:00+01:00' }, { start: '2017-03-25T12:00:00+01:00', end: '2017-03-25T13:00:00+01:00' }]
    Kloudless::Availability.expects(:find).with(@any_account, @any_calendar, second_call_without23, @duration)
                           .returns(two_slots)

    Scheduler.query_free_slots(@account, @duration, @date_from, @date_to)
  end
end
