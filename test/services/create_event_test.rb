require 'test_helper'

class CreateEventTest < ActiveSupport::TestCase
  include ActionMailer::TestHelper

  setup do
    @answers = OpenStruct.new(date: Time.utc(2011, 11, 11), email: 'laura@example.com', name: 'Laura')
    @account = accounts(:one)
    account_settings(:one).update!(account: @account, name: 'Rock law')

    time_windows = [{ start: @answers.date, end: @answers.date + @account.account_setting.duration.minutes }]
    Kloudless::Availability.stubs(:find).returns(time_windows)
  end

  test 'calls the provider that creates the event' do
    kaccount = Kloudless::Account.new(@account.remote_id, @account.auth_token)
    kcalendar = Kloudless::Calendar.new(@account.calendar_id, @account.calendar_name)
    event = Kloudless::Event.new("Cita con #{@answers.name}", kaccount, kcalendar, @answers.date, @answers.date + @account.account_setting.duration.minutes, @answers.name, @answers.email)

    Kloudless::Events.expects(:create).with(event)

    CreateEvent.call(@account, @answers)
  end

  test 'rescues errors from kloudless' do
    Kloudless::Events.stubs(:create).raises(Kloudless::Error)

    assert_raises(CreateEvent::CalendarError) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'calls the mailer to send the confirmation' do
    Kloudless::Events.stubs(:create)
    assert_enqueued_email_with(
      AppointmentMailer,
      :confirmation,
      args: {
        date: @answers.date.iso8601,
        email: @answers.email,
        firm_name: @account.account_setting.name,
        person_name: @answers.name,
        address: @account.account_setting.full_address
      }
    ) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'calls the mailer to report the account owner that a new appintment was created' do
    Kloudless::Events.stubs(:create)
    expected_email = @account.user.email
    assert_enqueued_email_with(
      AppointmentMailer,
      :created,
      args: { date: @answers.date.iso8601, email: expected_email, person_name: @answers.name, text: @answers.text }
    ) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'checks that the slot is still free' do
    kaccount = Kloudless::Account.new(@account.remote_id, @account.auth_token)
    kcalendar = Kloudless::Calendar.new(@account.calendar_id, @account.calendar_name)
    Kloudless::Events.stubs(:create)

    time_windows = [{ start: @answers.date, end: @answers.date + @account.account_setting.duration.minutes }]
    Kloudless::Availability.expects(:find).with(kaccount, kcalendar, time_windows, @account.account_setting.duration).returns(time_windows)

    CreateEvent.call(@account, @answers)
  end

  test 'raises exception is the slot is not free' do
    Kloudless::Availability.stubs(:find).returns([])

    assert_raises(CreateEvent::TimeNotAvailableError) do
      CreateEvent.call(@account, @answers)
    end
  end
end
