require 'test_helper'

class AppointmentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @account = accounts(:one)
    @account_setting = @account.account_setting
    @date_from = Time.now.utc.beginning_of_day.advance(days: 1)
    @date_to = @date_from + 5.days
  end

  test '#show calls the scheduler' do
    Scheduler.expects(:query_free_slots).with(@account, @account.account_setting.duration, @date_from, @date_to)
             .returns([Scheduler::TimeWindow.new(Time.now.utc, Time.now.utc)])

    get appointment_path(@account_setting.slug)
  end

  test '#show shows a page to select the appointment date and time' do
    start_next_period = @date_to + 1.day

    Scheduler.stubs(:query_free_slots)
             .returns([Scheduler::TimeWindow.new(@date_from, @date_from)])

    get appointment_path(@account_setting.slug)

    assert_select '.card-header', @account_setting.name
    assert_select '.day-interval', "#{I18n.l(@date_from, format: '%e %B')} - #{I18n.l(@date_to, format: '%e %B')}".strip
    assert_select "a[href='#{appointment_path(from: start_next_period.strftime('%Y-%m-%d'))}']"
    assert_select "button[data-value='#{@date_from.iso8601}']", count: 1, text: '00:00'
  end

  test '#show asks for the email and name after selecting date' do
    Scheduler.stubs(:query_free_slots)
             .returns([Scheduler::TimeWindow.new(@date_from, @date_from)])

    patch appointment_path(@account_setting.slug), params: { appointment: { 'selected-date' => @date_from.iso8601 } }

    assert_select '#appointment_email'
    assert_select '#appointment_name'
    assert_select '#appointment_text'
  end

  test '#show shows a confirmation message' do
    Scheduler.stubs(:query_free_slots)
             .returns([Scheduler::TimeWindow.new(@date_from, @date_from)])
    CreateEvent.stubs(:call)

    patch appointment_path(@account_setting.slug), params: { appointment: { 'selected-date' => @date_from.iso8601 } }
    patch appointment_path(@account_setting.slug), params: { appointment: { 'email' => 'help@email.com', 'name' => 'any name' } }

    assert_select '.card-accent-success'
  end
end
