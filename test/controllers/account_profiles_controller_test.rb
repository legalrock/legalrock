require 'test_helper'

class AccountProfilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    user = users(:one)
    @account = user.account
    sign_in(user)
  end

  test '#index shows the availability for each day of the week' do
    get account_profiles_url

    ap = AvailablePeriodForm.from_model(@account.available_period[0])
    assert_select('input:match("value", ?)', ap.start_time)
    assert_select('input:match("value", ?)', ap.end_time)
    assert_select('input[type="checkbox"][id="periods_1"][checked="checked"]')
  end

  test '#index renders not configured days' do
    get account_profiles_url

    assert_select('input[type="checkbox"][id="2"][checked="checked"]', 0)
    assert_select('input[type="checkbox"][id="3"][checked="checked"]', 0)
    assert_select('input[type="checkbox"][id="4"][checked="checked"]', 0)
    assert_select('input[type="checkbox"][id="5"][checked="checked"]', 0)
    assert_select('input[type="checkbox"][id="6"][checked="checked"]', 0)
    assert_select('input[type="checkbox"][id="7"][checked="checked"]', 0)
  end

  test '#index renders a field to configure the duration' do
    get account_profiles_url

    assert_select('input#duration_minutes')
  end

  test '#update shows errors if any' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => 'invalid_format' },
      duration: { 'minutes' => '5' }
    }

    assert_select('input.is-invalid')
    assert_select('.alert-danger')
  end

  test '#update success shows a notification' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' }
    }

    assert_select('.alert-success')
  end

  test '#update removes the success notification' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' }
    }

    assert_select('.alert-success')

    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => 'invalid', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' }
    }

    assert_select('.alert-success', false)
  end

  test '#update calls the service' do
    params = {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' }
    }

    UpdateAvailablePeriods.expects(:call).with(@account, params[:periods]).returns([])

    patch account_profile_url(@account), params: params
  end

  test '#update changes the duration' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' }
    }

    assert_equal(5, @account.account_setting.duration)
  end

  test '#update shows duration errors' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => 'invalid' }
    }

    assert_select('input.is-invalid')
    assert_select('.alert-danger')
  end

  test '#update changes the address' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' },
      address: 'street 91'
    }

    assert_equal('street 91', @account.account_setting.address)
  end

  test '#update shows address errors' do
    patch account_profile_url(@account), params: {
      periods: { '1' => '1', 'start_1' => '12:00', 'end_1' => '13:00' },
      duration: { 'minutes' => '5' },
      address: 'invalid' * 50
    }

    assert_select('input.is-invalid')
    assert_select('.alert-danger')
  end

  test 'redirects to connect calendar if there is no calendar' do
    @account.update(remote_id: nil, auth_token: nil)
    get account_profiles_url

    assert_redirected_to setup_account_index_path
  end
end
