require 'test_helper'

class Users::RegistrationControllerTest < ActionDispatch::IntegrationTest
  test '#create calls the service that creates the account' do
    CreateAccount.expects(:call).with('LRock Group').returns(accounts(:one))

    post user_registration_path params: { user: { name: 'LRock Group' } }

    assert_response(:success)
  end

  test '#create assigns the user to the account' do
    account = accounts(:one)
    CreateAccount.stubs(:call).returns(account)

    post user_registration_path params: { user: { name: 'LRock Group' } }

    assert(account.user)
  end
end
