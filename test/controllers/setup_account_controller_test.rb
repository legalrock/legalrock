require 'test_helper'

class SetupAccountControllerTest < ActionDispatch::IntegrationTest
  setup do
    Kloudless::Calendars.stubs(:find_all).returns([Kloudless::Calendar.new('id', 'name', 'zone')])

    @user = users(:one)
    @account = @user.account
    CreateAccount.stubs(:add_remote).returns(@account)
    sign_in(@user)
  end

  test '#index works' do
    get setup_account_index_url

    assert_response(:success)
  end

  test '#connect adds the remote settings' do
    CreateAccount.expects(:add_remote).with(@account, '1234', '5678').returns(@account)

    post setup_account_connect_path, params: { account: { id: '1234', access_token: '5678' } }
  end

  test '#connect shows the list of available calendars' do
    post setup_account_connect_path, params: { account: { id: '1234', access_token: '5678' } }

    assert_select('input[type="radio"][id="account_calendar_id_id"]')
  end

  test '#connect rescue errors retrieving the calendars' do
    Kloudless::Calendars.stubs(:find_all).raises(Kloudless::Error)

    post setup_account_connect_path, params: { account: { id: '1234', access_token: '5678' } }

    assert_select('.alert-danger')
    assert_select('button', false)
  end

  test '#connect rescue errors updating the account' do
    CreateAccount.stubs(:add_remote).raises(CreateAccount::Error)

    post setup_account_connect_path, params: { account: { id: '1234', access_token: '5678' } }

    assert_select('.alert-danger')
    assert_select('button', false)
  end

  test '#redirect to login if user is not already present' do
    sign_out(@user)

    get setup_account_index_url

    assert_redirected_to new_user_session_path
  end

  test '#calendar saves the selected calendar' do
    calendar = Kloudless::Calendar.new('cal_id1', 'name1', 'Europe/Madrid')
    Kloudless::Calendars.stubs(:find_all).returns([calendar, calendar])
    post setup_account_connect_path, params: { account: { id: '1234', access_token: '5678' } }

    post setup_account_calendar_path params: { account: { calendar_id: calendar.id, calendar_name: calendar.name, calendar_timezone: calendar.timezone } }

    @account.reload
    assert_equal('cal_id1', @account.calendar_id)
    assert_equal('name1', @account.calendar_name)
    assert_equal('Europe/Madrid', @account.account_setting.timezone)
  end

  test '#calendar redirects to the sign up' do
    post setup_account_connect_path, params: { account: { id: '1234', access_token: '5678' } }

    post setup_account_calendar_path params: { account: { calendar_id: 'anyid', calendar_name: 'any name', calendar_timezone: 'Europe/Madrid' } }

    assert_redirected_to account_profiles_path(welcome: true)
  end
end
