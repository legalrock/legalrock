require 'test_helper'
require 'webmock/minitest'
require 'kloudless'

class AvailabilityTest < ActiveSupport::TestCase
  setup do
    Kloudless.configure do |config|
      config.app_id = 'app_id'
    end
    @duration = 60
  end

  teardown do
    Kloudless.reset
  end

  test 'returns slots from a calendar' do
    account = Kloudless::Account.new('account_id', '[TOKEN]')
    calendar = Kloudless::Calendar.new('calendar_id', 'a_name')
    expected_request_body = {
      calendars: [calendar.id],
      meeting_duration: 'PT60M',
      constraints: {
        "time_windows": [{
          "start": '2001-02-03T00:00:00Z',
          "end": '2001-02-04T00:00:00Z'
        }]
      }
    }.to_json

    response_time_window = { 'start' => '2017-05-20T09:00:00+07:00', 'end' => '2017-05-20T11:00:00+07:00' }
    response_body = {
      time_windows: [response_time_window, response_time_window, response_time_window, response_time_window, response_time_window]
    }.to_json

    stub_request(:post, "https://api.kloudless.com/v1/accounts/#{account.id}/cal/availability")
      .with(
        body: expected_request_body,
        headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{account.auth_token}" }
      )
      .to_return(status: 200, body: response_body)

    result = Kloudless::Availability.find(account, calendar, [{ start: Time.utc(2001, 2, 3, 0, 0, 0), end: Time.utc(2001, 2, 4, 0, 0, 0) }], @duration)

    assert_equal(response_time_window, result[0])
  end

  test 'raises error when the connection fails' do
    account = Kloudless::Account.new('account_id', '[TOKEN]')
    calendar = Kloudless::Calendar.new('calendar_id', 'a_name')
    stub_request(:post, "https://api.kloudless.com/v1/accounts/#{account.id}/cal/availability").to_timeout

    assert_raises(Kloudless::NetException) do
      Kloudless::Availability.find(account, calendar, [{ start: Time.utc(2001, 2, 3, 0, 0, 0), end: Time.utc(2001, 2, 4, 0, 0, 0) }], @duration)
    end
  end

  test 'raises error when the response contains an error' do
    account = Kloudless::Account.new('account_id', '[TOKEN]')
    calendar = Kloudless::Calendar.new('calendar_id', 'a_name')
    stub_request(:post, "https://api.kloudless.com/v1/accounts/#{account.id}/cal/availability")
      .to_return(status: 200, body: { error_code: '404' }.to_json)

    assert_raises(Kloudless::Error) do
      Kloudless::Availability.find(account, calendar, [{ start: Time.new(2001, 2, 3, 0, 0, 0), end: Time.new(2001, 2, 4, 0, 0, 0) }], @duration)
    end
  end
end
