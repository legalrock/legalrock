require 'test_helper'
require 'webmock/minitest'
require 'kloudless'

class AuthTest < ActiveSupport::TestCase
  setup do
    Kloudless.configure do |config|
      config.app_id = 'app_id'
    end
  end

  teardown do
    Kloudless.reset
  end

  test 'verifying the token' do
    response_body = {
      client_id: Kloudless.configuration.app_id,
      account_id: '4567'
    }.to_json

    stub_request(:get, 'https://api.kloudless.com/v1/oauth/token')
      .with(
        headers: { 'Content-Type' => 'application/json', 'Authorization' => 'Bearer access_token' }
      )
      .to_return(status: 200, body: response_body)

    Kloudless::Auth.verify_token('4567', 'access_token')
  end

  test 'raises error when the app_id does not match' do
    response_body = {
      client_id: '1234',
      account_id: '4567'
    }.to_json

    stub_request(:get, 'https://api.kloudless.com/v1/oauth/token')
      .with(
        headers: { 'Content-Type' => 'application/json', 'Authorization' => 'Bearer access_token' }
      )
      .to_return(status: 200, body: response_body)

    assert_raises(Kloudless::Error) { Kloudless::Auth.verify_token('1234', 'access_token') }
  end

  test 'raises error when the connection fails' do
    stub_request(:get, 'https://api.kloudless.com/v1/oauth/token').to_timeout

    assert_raises(Kloudless::NetException) { Kloudless::Auth.verify_token('4567', 'access_token') }
  end

  test 'raises error when the response contains an error' do
    stub_request(:get, 'https://api.kloudless.com/v1/oauth/token')
      .to_return(status: 400, body: { error: 'invalid_token' }.to_json)

    assert_raises(Kloudless::Error) { Kloudless::Auth.verify_token('4567', 'access_token') }
  end
end
