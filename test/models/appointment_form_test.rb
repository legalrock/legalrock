require 'test_helper'

class AppointmentFormTest < ActiveSupport::TestCase
  test 'validates format of date' do
    appointment = AppointmentForm.new
    appointment.date = 'invalid'

    appointment.valid?

    assert_equal(:invalid, appointment.errors.details[:date].first[:error])
  end

  test 'initializes values from a hash' do
    form = AppointmentForm.new
    form.read_params(
      'selected-date' => '2019-03-20T18:00:00+01:00',
      'name' => 'any name',
      'email' => 'any email',
      'text' => 'any text'
    )

    assert_equal('any name', form.to_h[:name])
    assert_equal('any email', form.to_h[:email])
    assert_equal('2019-03-20T18:00:00+01:00', form.to_h[:date])
    assert_equal('any text', form.to_h[:text])
  end
end
