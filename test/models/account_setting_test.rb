require 'test_helper'

class AccountSettingTest < ActiveSupport::TestCase
  test 'validates timezone' do
    setting = AccountSetting.new(timezone: 'INVALID')

    refute(setting.valid?)
    assert_equal(:invalid, setting.errors.details[:timezone][0][:error])
  end

  test 'generates and saves the slug if the name is changed' do
    account = accounts(:one)
    setting = AccountSetting.new(account: account, timezone: 'Europe/Madrid', name: 'the legal rock')
    setting.save!

    assert_equal('the-legal-rock', setting.slug)

    setting.update(name: 'another name')

    assert_equal('another-name', setting.slug)
  end

  test 'slugs must be unique' do
    account = accounts(:one)
    AccountSetting.create(account: account, timezone: 'Europe/Madrid', name: 'the legal rock')

    another_account = accounts(:two)
    setting = AccountSetting.create(account: another_account, timezone: 'Europe/Madrid', name: 'the legal rock')

    assert_equal("the-legal-rock-#{another_account.id}", setting.slug)
  end
end
